const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "linkcm",
    projectName: "react-app-1",
    webpackConfigEnv,
    argv,
  });

  const webpackConf = merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
  });

  console.log("webpack --> ", webpackConf);
  return webpackConf;
};
